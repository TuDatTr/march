#!/bin/bash
set -o errexit -o pipefail -o noclobber -o nounset

! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo "I'm sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=t:l:h:u:s:r:c:p:
LONGOPTS=timezone:,locale:,hostname:,username:,sudogrp:,rootpart:,cryptroot:,password:

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"

timezone="CHANGEME" # specified in ./march-chroot.sh
locale="CHANGEME" # specified in ./march-chroot.sh
hostname="CHANGEME" # specified in ./march-chroot.sh
username="CHANGEME" # specified in ./march-chroot.sh
sudogrp="CHANGEME" # specified in ./march-chroot.sh
rootpart="CHANGEME" # specified in ./march-chroot.sh
cryptroot="CHANGEME" # specified in ./march-chroot.sh
password="CHANGEME"  # specified in ./march-chroot.sh

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

while true; do
    case "$1" in
        -p|--password)
            password="$2"
            shift 2
            ;;
        -t|--timezone)
            timezone="$2"
            shift 2
            ;;
        -l|--locale)
            locale="$2"
            shift 2
            ;;
        -h|--hostname)
            hostname="$2"
            shift 2
            ;;
        -u|--username)
            username="$2"
            shift 2
            ;;
        -s|--sudogrp)
            sudogrp="$2"
            shift 2
            ;;
        --help)
            echo "
 Usage: ./install.sh -d [DEVICE] -t [/usr/share/zoneinfo/REGION/CITY] -l [LOCALE] -h [HOSTNAME] -u [USER] -s [SUDO]
A small script to automate my archlinux configuration.

Mandatory arguments:
-t | --timezone     Your timezone according to /usr/share/zoneinfo/ e.g. "/usr/share/zoneinfo/Europe/Berlin"
-l | --locale       Your locale according to /etc/locale.gen e.g. "en_US.UTF-8 UTF-8"
-h | --hostname     Name for your machine e.g. "ArchLinux"
-u | --username     Your username e.g. "foo"
-s | --sudogrp      Name of the sudogrp
-h | --help         This here.
            "
            exit 0
            ;;
        -r|--rootpart)
            rootpart="$2"
            shift 2
            ;;
        -c|--cryptroot)
            cryptroot="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
        esac
    done


ln -sf "$timezone" /etc/localtime

hwclock --systohc

echo "$locale" >> /etc/locale.gen
if [ "$locale" != "en_US.UTF-8 UTF-8" ]; then
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
fi

locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "$hostname" > /etc/hostname

sed -i "s/^HOOKS=([[:alnum:][:space:]]\+)/HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt filesystems fsck)/" /etc/mkinitcpio.conf
mkinitcpio -p linux

passwd

bootctl install
echo "timeout 3
default arch" >| /boot/loader/loader.conf
echo "title ArchLinux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options rw cryptdevice=$(blkid $rootpart | cut -d" " -f2 | tr -d \"):cryptroot root=$cryptroot i8042.noloop i8042.nomux i8042.nopnp i8042.reset" > /boot/loader/entries/arch.conf

sed -i "s/march-reboot.sh/\0 -u \"$username\" -s \"$sudogrp\" -p \"$password\"/" /etc/systemd/system/march.service

systemctl enable march.service
systemctl enable NetworkManager.service
systemctl enable dhcpcd.service
