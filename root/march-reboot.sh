#!/bin/bash
set -o errexit -o pipefail -o noclobber -o nounset

! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo "I'm sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=u:s:p:
LONGOPTS=username:,sudogrp:,password:

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"

username="CHANGEME"
password="CHANGEME"
sudogrp="CHANGEME"

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

while true; do
    case "$1" in
        -u|--username)
            username="$2"
            shift 2
            ;;
        -p|--password)
            password="$2"
            shift 2
            ;;
        -s|--sudogrp)
            sudogrp="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

sleep 5
pacman -Sy
pacman -S --noconfirm git zsh gvim wget arch-audit lightdm lightdm-webkit2-greeter xorg-server i3-gaps xorg-xbacklight xorg-xrandr i3lock-color alsa-utils pulseaudio pulseaudio-alsa pulseaudio-bluetooth playerctl pavucontrol exfat-utils udiskie  blueman acpi acpid tlp compton conky rxvt-unicode  networkmanager network-manager-applet networkmanager-openconnect networkmanager-openvpn stow maim openssh xdotool xclip thunderbird firefox veracrypt keepassxc evince pandoc pdfgrep adobe-source-han-sans-otc-fonts python3 python-pip  ranger atool elinks ffmpegthumbnailer highlight libcaca lynx mediainfo odt2txt perl-image-exiftool poppler python-chardet transmission-cli w3m texlive-most byobu rofi nextcloud intel-ucode xf86-video-intel 

echo "%$sudogrp ALL=(ALL) ALL" >> /etc/sudoers
groupadd "$sudogrp"
useradd -m -G "$sudogrp" -s /bin/zsh "$username"
echo "$username:$password" | chpasswd

systemctl enable lightdm
sed -i "s/#user-session.*/user-session=i3/g" /etc/lightdm/lightdm.conf
sed -i "s/#greeter-session.*/greeter-session=lightdm-webkit2-greeter/g" /etc/lightdm/lightdm.conf

cp $CWD/etc/systemd/system/suspend@.service /etc/systemd/system/suspend@.service
cp $CWD/etc/X11/xorg.conf.d/30-touchpad.conf /etc/X11/xorg.conf.d/30-touchpad.conf
cp $CWD/etc/iptables/iptables.rules /etc/iptables/iptables.rules

# From this on we need need to be $username
su $username sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" &

su $username sh -c "git clone https://github.com/powerline/fonts.git --depth=1 /home/$username/fonts"
su $username sh -c "chmod +x /home/$username/fonts/install.sh"
su $username sh -c "/home/$username/fonts/install.sh"
su $username sh -c "rm -rf /home/$username/fonts/"

su $username sh -c "git clone https://gitlab.com/tudattr/dotfiles ./home/$username/.dotfiles"
su $username sh -c "stow -d /home/$username/.dotfiles -t /home/$username vim"
rm /home/$username/.zshrc
su $username sh -c "stow -d /home/$username/.dotfiles -t /home/$username zsh"
su $username sh -c "stow -d /home/$username/.dotfiles -t /home/$username config"

systemctl disable march

shutdown -r now
