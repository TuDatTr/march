#!/bin/bash
set -o errexit -o pipefail -o noclobber -o nounset

! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo "I'm sorry, `getopt --test` failed in this environment."
    exit 1
fi

OPTIONS=d:t:l:h:u:s:p:
LONGOPTS=device:,timezone:,locale:,hostname:,username:,sudogrp:,help,password:

! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"

device="CHANGEME" # Device that shoud be partitioned e.g. "/dev/sda"
timezone="CHANGEME" # Your timezone according to /usr/share/zoneinfo/ e.g. "/usr/share/zoneinfo/Europ/Berlin"
locale="CHANGEME" # Your locale according to /etc/locale.gen e.g. "en_US.UTF-8 UTF-8"
hostname="CHANGEME" # Name for your machine e.g. "ArchLinux"
username="CHANGEME" # Your username e.g. "foo"
sudogrp="CHANGEME" # Name of the sudogrp

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

while true; do
    case "$1" in
        -d|--device)
            device="$2"
            shift 2
            ;;
        -t|--timezone)
            timezone="$2"
            shift 2
            ;;
        -l|--locale)
            locale="$2"
            shift 2
            ;;
        -h|--hostname)
            hostname="$2"
            shift 2
            ;;
        -u|--username)
            username="$2"
            shift 2
            ;;
        -p|--password)
            password="$2"
            shift 2
            ;;
        -s|--sudogrp)
            sudogrp="$2"
            shift 2
            ;;
        --help)
            echo "
Usage: ./install.sh -d [DEVICE] -t [/usr/share/zoneinfo/REGION/CITY] -l [LOCALE] -h [HOSTNAME] -u [USER] -s [SUDO]
A small script to automate my archlinux configuration.

Mandatory arguments:
-d | --device       Device that shoud be partitioned e.g. "/dev/sda"
-t | --timezone     Your timezone according to /usr/share/zoneinfo/ e.g. "/usr/share/zoneinfo/Europe/Berlin"
-l | --locale       Your locale according to /etc/locale.gen e.g. "en_US.UTF-8 UTF-8"
-h | --hostname     Name for your machine e.g. "ArchLinux"
-u | --username     Your username e.g. "foo"
-s | --sudogrp      Name of the sudogrp
-h | --help         This here.
            "
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
        esac
    done

# partition 1: 1MiB -> 500MiB FAT32
# partition 2: 500MiB -> 3GiB swap
# partition 3: 3GiB -> ..     ext4
parted $device mklabel gpt mkpart primary fat32 1MiB 512 mkpart primary linux-swap 500MiB 3GiB mkpart primary ext4 3GiB 100% set 1 esp on -s

if [ -b "$device\p1" ]; then
    bootpart=$device\p1
    swappart=$device\p2
    rootpart=$device\p3
else
    bootpart=$device\1
    swappart=$device\2
    rootpart=$device\3
fi

mkfs.vfat $bootpart
mkswap $swappart
swapon $swappart

# LUKS
cryptsetup -y luksFormat --type luks2 $rootpart
cryptsetup open $rootpart cryptroot

cryptroot=/dev/mapper/cryptroot

mkfs.ext4 $cryptroot

mount  $cryptroot /mnt
mkdir /mnt/boot
mount $bootpart /mnt/boot

pacstrap /mnt base base-devel linux linux-firmware networkmanager dhcpcd

genfstab -U /mnt >| /mnt/etc/fstab

cp $CWD/etc/systemd/system/march.service /mnt/etc/systemd/system/march.service
cp $CWD/root/march-chroot.sh /mnt/root/march-chroot.sh
cp $CWD/root/march-reboot.sh /mnt/root/march-reboot.sh
chmod +x /mnt/root/march-chroot.sh
chmod +x /mnt/root/march-reboot.sh

mkdir -p /mnt/root/etc/systemd/system
mkdir -p /mnt/root/etc/X11/xorg.conf.d
mkdir -p /mnt/root/etc/iptables
cp $CWD/etc/systemd/system/suspend@.service /mnt/root/etc/systemd/system/suspend@.service
cp $CWD/etc/X11/xorg.conf.d/30-touchpad.conf /mnt/root/etc/X11/xorg.conf.d/30-touchpad.conf
cp $CWD/etc/iptables/iptables.rules /mnt/root/etc/iptables/iptables.rules
chmod +x /mnt/root/march-reboot.sh

arch-chroot /mnt /root/march-chroot.sh -t "$timezone" -l "$locale" -h "$hostname" -u "$username" -s "$sudogrp" -c "$cryptroot" -r "$rootpart" -p "$password"

umount $bootpart
swapoff $swappart
umount $cryptroot
cryptsetup close $cryptroot

shutdown -r now
